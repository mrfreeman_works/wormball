local storage = wormball.storage


wormball.leaderboard = {}











    -- get highscores from storage if found. if not found, create an empty table for each existing arena.


if arena_lib.mods and arena_lib.mods["wormball"] and arena_lib.mods["wormball"].arenas then
  
    for id, arena in pairs(arena_lib.mods["wormball"].arenas) do

        local arena_name = arena.name

        -- assume the leaderboard is empty
        local leaderboard = {}

        -- recall the leaderboard from storage
        if storage:get_string(arena_name .. "_highscores") then

            local ser_leaderboard = storage:get_string(arena_name .. "_highscores")
            
            if ser_leaderboard then 
                leaderboard = minetest.deserialize( ser_leaderboard ) 
            end

        end

        -- update storage
        storage:set_string(arena_name .. "_highscores", minetest.serialize(leaderboard))
        -- update the arena leaderboard
        arena_lib.mods["wormball"].arenas[id].singleplayer_leaderboard = leaderboard
        
    end

end
  


-- returns a , b
-- a can be true or false, indicates success
-- b is the error message

wormball.leaderboard.save_highscores = function(arena_name,highscores)
    local arenas = arena_lib.mods["wormball"].arenas
    if # arenas == 0 then return false, "there are no wormball arenas" end

    if not arena_lib.get_arena_by_name("wormball",arena_name) then return false, "there is no such arena" end

    local id , arena = arena_lib.get_arena_by_name("wormball",arena_name)

    -- update storage
    storage:set_string(arena_name .. "_highscores", minetest.serialize(highscores))
    -- update the arena leaderboard
    arena_lib.mods["wormball"].arenas[id].singleplayer_leaderboard = highscores

    return true , "success"

end

-- returns: a , b , c
-- a can be nil or a table of highscores
-- b is either true or false, indicates if a is a table
-- c is the error message

wormball.leaderboard.get_highscores = function(arena_name)

    local arenas = arena_lib.mods["wormball"].arenas
    if # arenas == 0 then return nil, false, "there are no arenas" end

    if not arena_lib.get_arena_by_name("wormball",arena_name) then return nil, false, "there is no such arena" end

    local id , arena = arena_lib.get_arena_by_name("wormball",arena_name)

    local highscores = {}

    local msg = "there were no highscores to return"


    local ser_leaderboard = storage:get_string(arena_name .. "_highscores")
        
    if ser_leaderboard then 
        leaderboard = minetest.deserialize( ser_leaderboard ) 
        if leaderboard then
            highscores = leaderboard
            msg = "success!"
        end
    end

    return highscores, true, msg

end


-- returns a , b
-- a can be true or false, indicates success
-- b is the error message


wormball.leaderboard.clear_highscores = function(arena_name)

    local arenas = arena_lib.mods["wormball"].arenas
    if # arenas == 0 then return false, "there are no wormball arenas" end

    if not arena_lib.get_arena_by_name("wormball",arena_name) then return false, "there is no such arena" end

    local id , arena = arena_lib.get_arena_by_name("wormball",arena_name)

    -- update storage
    storage:set_string(arena_name .. "_highscores", minetest.serialize({}))
    -- update the arena leaderboard
    arena_lib.mods["wormball"].arenas[id].singleplayer_leaderboard = {}

    return true , "success"

end





-- scoreboards

wormball.show_leaderboard_formspec = function(playername,arena_name,board_title,leaderboard, formname)

    if not minetest.get_player_by_name(playername) then 
        return false, "no player by that name!" 
    end

    local is_empty = false 
    local player_list = ""

    if not leaderboard or not leaderboard[1] then 
        is_empty = true 
        leaderboard = {}
    end

    if is_empty == false then
        
        for idx , winner_table in ipairs( leaderboard ) do
            local max_len = 30
            local p_name = winner_table [ 2 ]
            local name_len = string.len( winner_table [ 2 ] )
            if name_len > 20 then
                p_name = string.sub(p_name, 1, 20)
                name_len = 20
            end
            local pts = tostring( winner_table [ 1 ] )
            local pts_len = string.len( pts )
            local spaces = 5 - pts_len

            player_list = player_list .. pts
            
            for i = 1, spaces do
                player_list = player_list .. " "
            end

            player_list = player_list .. p_name .. ","

        end
    end

    local formspec = "formspec_version[4]"..
    "size[8,9]"..
    "box[0,0;8,1;#ff4800]"..
    "label[3,0.5;" .. board_title .. "]"..
    "box[0,1;8,1;#ff8000]"..
    "label[3,1.5;" .. arena_name .. "]"..
    "box[0,2;8,7;#191359]"..
    "textlist[0.5,2.5;7,6;;".. player_list ..";1;true]"

    minetest.show_formspec(playername, formname, formspec)

    return true, ""

end




wormball.show_multi_scores = function( arena , player , scores )

    local leaderboard = scores
    --minetest.chat_send_all(dump(arena))
    local player_list = ""

    wormball.show_leaderboard_formspec(player,arena.name,"Multiplayer Scores",leaderboard,'multiscoreboard')
    return true , ""

end


wormball.show_singleplayer_leaderboard = function( arena_name , playername )

    if not arena_lib.get_arena_by_name('wormball', arena_name ) then return false, "no arena by that name!" end
    local arena_id, arena = arena_lib.get_arena_by_name('wormball', arena_name )
    local leaderboard = arena.singleplayer_leaderboard
    --minetest.chat_send_all(dump(arena.singleplayer_leaderboard))
    
    wormball.show_leaderboard_formspec(playername,arena_name,"LeaderBoard",leaderboard,'scoreboard')
    return true , ""
end




