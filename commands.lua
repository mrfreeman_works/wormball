--thx AliasAlreadyTaken for this version cmd
minetest.register_chatcommand("wormball_version", {
    description = "Shows wormball version",
    privs = {
        interact = true,
    },
    func = function(name, param)
        return true, "Wormball Version: "..wormball.version
      
    end
  })



ChatCmdBuilder.new("wormball", function(cmd)

  -- create arena
  cmd:sub("create :arena", function(name, arena_name)
      arena_lib.create_arena(name, "wormball", arena_name)
  end)

  cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
      arena_lib.create_arena(name, "wormball", arena_name, min_players, max_players)
  end)

  -- remove arena
  cmd:sub("remove :arena", function(name, arena_name)
      arena_lib.remove_arena(name, "wormball", arena_name)
  end)

  -- list of the arenas
  cmd:sub("list", function(name)
      arena_lib.print_arenas(name, "wormball")
  end)

  -- enter editor mode
  cmd:sub("edit :arena", function(sender, arena)
      arena_lib.enter_editor(sender, "wormball", arena)
  end)

  -- enable and disable arenas
  cmd:sub("enable :arena", function(name, arena)
      arena_lib.enable_arena(name, "wormball", arena)
  end)

  cmd:sub("disable :arena", function(name, arena)
      arena_lib.disable_arena(name, "wormball", arena)
  end)

  --clear highscores
  cmd:sub("clrleaderboard :arena", function(name, arena)
    local success, msg = wormball.leaderboard.clear_highscores(arena)
    if success == true then 
        return "[!] Wormball arena " .. arena .. " Highscores Cleared!"
    else
        return "[!] Highscore Clear Attempt Failed! Error: ".. msg
    end

  end)

  cmd:sub("leaderboard :arena :p_name:username", function(name, arena, p_name)
    if not wormball.show_singleplayer_leaderboard( arena , p_name ) then
        return "Invalid usage: please specify a valid wormball arena name to view its leaderboard."
    end
  end)

end, {
  description = [[

    (/help wormball)

    Use this to configure your arena:
    - create <arena name> [min players] [max players]
    - edit <arena name>
    - enable <arena name>
    - list -- show created arenas
    - clrleaderboard <arena_name>

    Other commands:
    - remove <arena name>
    - disable <arena>
    ]],
  privs = { wormball_admin = true }
})


ChatCmdBuilder.new("wormball", function(cmd)

  -- create arena
  cmd:sub("create :arena", function(name, arena_name)
      arena_lib.create_arena(name, "wormball", arena_name)
  end)

  cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
      arena_lib.create_arena(name, "wormball", arena_name, min_players, max_players)
  end)

  -- remove arena
  cmd:sub("remove :arena", function(name, arena_name)
      arena_lib.remove_arena(name, "wormball", arena_name)
  end)

  -- list of the arenas
  cmd:sub("list", function(name)
      arena_lib.print_arenas(name, "wormball")
  end)

  -- enter editor mode
  cmd:sub("edit :arena", function(sender, arena)
      arena_lib.enter_editor(sender, "wormball", arena)
  end)

  -- enable and disable arenas
  cmd:sub("enable :arena", function(name, arena)
      arena_lib.enable_arena(name, "wormball", arena)
  end)

  cmd:sub("disable :arena", function(name, arena)
      arena_lib.disable_arena(name, "wormball", arena)
  end)

  cmd:sub("leaderboard :arena :player", function(name, arena, player)
    wormball.show_singleplayer_leaderboard( arena , player )
  end)

  --clear highscores
  cmd:sub("clrleaderboard :arena", function(name, arena)
    local success, msg = wormball.leaderboard.clear_highscores(arena)
  end)


end, {
  description = [[

    (/help wormball)

    Use this to configure your arena:
    - create <arena name> [min players] [max players]
    - edit <arena name>
    - enable <arena name>
    - list -- show created arenas
    - clrleaderboard <arena_name>

    Other commands:
    - remove <arena name>
    - disable <arena>
    ]],
  privs = { wormball_admin = true }
})




minetest.register_chatcommand("wormball_leaderboard", {
	params = "<arena_name>",
	description = [[
        View the singleplayer leaderboard of a wormball arena
        - arena_name -- must be a valid wormball arena name
    ]],
	privs = {},
	func = function( name , arena_name)
        if not wormball.show_singleplayer_leaderboard( arena_name , name ) then
            return "Invalid usage: please specify a valid wormball arena name to view its leaderboard."
        end
	end,
})







minetest.register_chatcommand("wormball_multiscores", {
	params = "<arena_name>",
	description = [[
        View the scores of players of the last game played in a wormball arena
        - arena_name -- must be a valid wormball arena name
    ]],
	privs = {},
	func = function( name , arena_name)
        if not arena_lib.get_arena_by_name('wormball', arena_name ) then return "Invalid usage: please specify a valid wormball arena name to view its leaderboard." end
        local arena_id, arena = arena_lib.get_arena_by_name('wormball', arena_name )
        if not arena.multi_scores or arena.multiscores == {} then return "no scores found!" end
        if not wormball.show_multi_scores( arena , name , arena.multi_scores ) then
            return "Invalid usage: please specify a valid wormball arena name to view its leaderboard."
        end
        return "error"
	end,
})


