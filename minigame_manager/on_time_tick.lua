local T = minetest.get_translator("wormball")
local function eliminate_pl(arena , pl_name , pl_score)
    local color = arena.players[ pl_name ].color
    minetest.chat_send_player(pl_name, T("Game Over! You were eliminated with @1 pts", pl_score)..".")    
    
    --this is sufficent to kill them on the next globalstep              
    arena.players[pl_name].eliminated = true
    arena.players[pl_name].alive = false

    
end






arena_lib.on_time_tick('wormball', function(arena)
    
    ----------------------------------------------
    ----------------------------------------------
    --  send HUD with highscore and scores of current players
    ----------------------------------------------
    ----------------------------------------------
    local string = ""
    if arena.mode == 'singleplayer' and arena.singleplayer_leaderboard and arena.singleplayer_leaderboard [1] then
        local leader = arena.singleplayer_leaderboard[1]
        if leader then
            string = T("highscore") ..":".. leader[1] .. "\n"
        end
    end

    local scores


    for pl_name, stats in pairs(arena.players) do
        if stats.score then
            local score_info = {pl_name,stats.score}
            if not scores then 
                scores = {score_info}
            else
                local inserted = false
                for idx , score_tbl in ipairs(scores) do
                    if score_info[2] >= score_tbl[2] then
                        table.insert(scores,idx,score_info)
                        inserted = true
                        break
                    end                    
                end
                if not inserted then
                    table.insert(scores,#scores + 1, score_info)
                end
            end
        end
    end

    --minetest.chat_send_all(dump(scores))

    if scores then
        for idx,score_tbl in ipairs(scores) do
            string = string .. score_tbl[1] .. ": ".. score_tbl[2] .. "\n"
        end
    end

    for pl_name,stats in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        if not wormball.HUD[pl_name] then
            
            local new_hud = {}
            new_hud.scores = player:hud_add({
                hud_elem_type = "text",
                position = {x = 0, y = 1},
                name = "wormball_highscores",
                text = string,
                scale = {x = 3, y = 3},
                alignment = {x = 1, y = -1},
                offset = {x = 5, y = -5}
            })
            wormball.HUD[pl_name] = new_hud
        else
            local id = wormball.HUD[pl_name].scores
            player:hud_change(id, "text", string)
        end
    end


    --minetest.chat_send_all( dump(arena.multi_scores))


                

    ------------------------------------------------------------------
    --==============================================================--
    --========= If in multiplayer, eliminate 1 player every so often
    --==============================================================--
    ------------------------------------------------------------------

    if arena.mode == 'multiplayer' and scores and #scores > 1 then --
        arena.time_to_next_elim = arena.time_to_next_elim - 1
        --minetest.chat_send_all ('we got to here')

        --eliminate a player
        if arena.time_to_next_elim == 0 then

            arena.elims = arena.elims + 1

            arena.time_to_next_elim = 90

            if arena.elims > 1 then
                arena.time_to_next_elim = 60
            end

            if arena.elims > 2 then
                arena.time_to_next_elim = 55
            end

            if arena.elims > 3 then
                arena.time_to_next_elim = 50
            end

            if arena.elims > 4 then
                arena.time_to_next_elim = 45
            end

            if arena.elims > 5 then
                arena.time_to_next_elim = 30
            end
            

            local elim_players = {} 


            -- by default, eliminate the player with the least score
            -- if other players have the same score, and there isnt a tie for first place, then they all get eliminated.

            local pl_name = scores[#scores][1]
            local pl_score = scores[#scores][2]         

            for i = # scores , 1 , -1 do 

                if i == # scores then

                    elim_players = { { pl_name , pl_score } }

                elseif scores [ i ][ 2 ] == pl_score then

                    table.insert( elim_players , { scores [ i ][ 1 ] , scores [ i ][ 2 ] } )

                end

            end


            
            -- if there is a tie with first place, dont eliminate, do a tiebreaker instead.
            if scores [ # scores ][ 2 ] == scores [ 1 ][ 2 ] then

                elim_players = {} 
                arena.time_to_next_elim = 10
                arena_lib.HUD_send_msg_all('broadcast', arena, "Elimination Tied! 10 sec Tiebreaker!", 2,nil,0xFFFF00)

            end

            if # elim_players > 0 then   
                
                local elim_string = ""
                local elim_num = 0
                for _ , tbl in pairs( elim_players ) do

                    eliminate_pl( arena , tbl [ 1 ] , tbl [ 2 ] )
                    elim_string = elim_string .. tbl [ 1 ]
                    if #elim_players > 1 then
                        elim_string = elim_string .. ", "
                    end
                    elim_num = elim_num + 1

                end    

                if elim_num == 1 then

                    elim_string = elim_string .. " was "

                else

                    elim_string = elim_string .. " were "

                end

                
                            

                arena_lib.HUD_send_msg_all('broadcast', arena, elim_string .. T("eliminated with @1 pts", pl_score), 2,nil,0xFFFF00)
            
            end
            
        end
                      

        local c = 0x00FF00
        if arena.time_to_next_elim <= 10 then
            c = 0xFFFF00
        end
        if arena.time_to_next_elim <= 5 then
            c = 0xFF0000
        end

        if arena.current_time > 5 then
            arena_lib.HUD_send_msg_all('hotbar', arena, T("Next Elimination In").. " " .. arena.time_to_next_elim, 1,nil,c)
        end


    end

    
    ----------------------------------------------
    ----------------------------------------------
    --  set powerups within the arena   ----------
    ----------------------------------------------
    ----------------------------------------------


    -- get the arena area points from settings, order them
    local p1 = arena.area_to_clear_after_game_pos_1
    local p2 = arena.area_to_clear_after_game_pos_2
    local x1 = p1.x 
    local x2 = p2.x
    local y1 = p1.y 
    local y2 = p2.y
    local z1 = p1.z 
    local z2 = p2.z
    if x2 < x1 then
        local temp = x2
        x2 = x1
        x1 = temp
    end
    if y2 < y1 then
        local temp = y2
        y2 = y1
        y1 = temp
    end
    if z2 < z1 then
        local temp = z2
        z2 = z1
        z1 = temp
    end


    --get a local table of color names
    local color_table = wormball.color_names
    

    --get current number of players
    local num_players = 0
    for pl_name,stats in pairs(arena.players) do
        num_players = num_players +1
    end


    --decide whether to (have a chance of) removing dots
    local remove = false
    if #arena.dots and #arena.dots> arena.min_food_factor * num_players + arena.min_food then
        remove = true
    end


    --add dots, with greater chance with more players
    for pl_name,stats in pairs(arena.players) do
        --random location within arena
        local rand_pos = {x = math.random(x1,x2),y = math.random(y1,y2), z=math.random(z1,z2)}
        local item = 'none'
        --random chance to place random color
        if math.random(1,3)== 1 then
            local color = color_table[math.random(1,#color_table)]
            item = "wormball:power_"..color
        end
        --if you want to place other nodes instead/also, set item here...


        if item ~= 'none' then
            if minetest.get_node(rand_pos).name == 'air' then --only place in air (not replace arena or worms or other powerups)
                minetest.set_node(rand_pos, {name=item})
                table.insert(arena.dots,rand_pos) --keep track of where powerups (dots) are
            end
        end
        if remove then
            --if there are enough powerups to remove some, have a 0.5 chance of removing them
            if math.random(1,2) == 1 then
                local rem_pos = table.remove(arena.dots,math.random(4,#arena.dots)) --forget the pos of removed powerup
                if not(string.find(minetest.get_node(rem_pos).name,"wormball:straight_")) 
                and not(string.find(minetest.get_node(rem_pos).name,"wormball:corner_"))
                and not(string.find(minetest.get_node(rem_pos).name,"wormball:head_")) then

                    minetest.set_node(rem_pos, {name="air"})

                end
            end
        end
    end
end)



