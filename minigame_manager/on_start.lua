
-- counting the initial number of players in the game has been moved to an on_start function because the highscore info 
-- returned nil in a crash report. Perhaps not all players are accounted for in on_load. This ensures correct census info.
arena_lib.on_start('wormball', function(arena)
    local c = 0
    for pl_name, stats in pairs(arena.players) do
        c =c +1
    end
    arena.num_players = c

    
    --clear the board of gamepieces
    local pos1 = arena.area_to_clear_after_game_pos_1
    local pos2 = arena.area_to_clear_after_game_pos_2
    local x1 = pos1.x
    local x2 = pos2.x
    local y1 = pos1.y
    local y2 = pos2.y 
    local z1 = pos1.z 
    local z2 = pos2.z 
    if x1 > x2 then
        local temp = x2
        x2 = x1
        x1 = temp
    end
    if y1 > y2 then
        local temp = y2
        y2 = y1
        y1 = temp
    end
    if z1 > z2 then
        local temp = z2
        z2 = z1
        z1 = temp
    end

    for x = x1,x2 do
        for y = y1,y2 do
            for z = z1,z2 do

                --only remove wormball-registered nodes
                local nodename = minetest.get_node({x=x,y=y,z=z}).name 
                if string.find(nodename,'wormball') then
                    minetest.set_node({x=x,y=y,z=z}, {name="air"})
                end
                
            end
        end
    end


    -- set players' attachment entities

    for pl_name, stats in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        local pos = player:get_pos()
        local att = minetest.add_entity(pos, 'wormball:player_att')
        player:set_attach(att, "", {x=0,y=0,z=0}, {x=0,y=0,z=0})
        arena.players[pl_name].attached = true --indicates that the globalstep may take control; the player is supposed to be attached
    end




end)