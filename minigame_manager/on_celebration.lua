-- scoreboards
local storage = wormball.storage
local T = minetest.get_translator("wormball")

arena_lib.on_celebration('wormball', function(arena, winner_name)

    --reset player textures back to the texture they were... (wormball sets player textures to clear)

    for pl_name,stats in pairs(arena.players) do
        if not(arena_lib.is_player_spectating(pl_name)) then
            wormball.detach(pl_name)
        end
    end

    

    --for now, arena_lib only supports single winners... change this when it supports multiple winners

    if type(winner_name) == 'string' then


        if arena.mode == 'singleplayer' then

            -- leaderboard will be a table with the following format 
            -- {
            --    {score,plname}
            --    {score,plname}
            --   }

            -- it will always be ordered, with the highest score first.

            local new_highscore = false

            local leaderboard = wormball.leaderboard.get_highscores(arena.name) --arena.singleplayer_leaderboard

            local score = arena.players [ winner_name ] .score

            if not leaderboard then leaderboard = {} end

            if # leaderboard == 0 then
                leaderboard [ 1 ] = { score , winner_name }
                new_highscore = true

            else

                local insert = true

                for idx , winner_table in ipairs( leaderboard ) do

                    if winner_table [ 2 ] == winner_name then --if the player already has a score on the board...

                        if score > winner_table[1] then -- new higher score achieved... we will now remove the old score so we can input the new score at the correct location
                            table.remove(leaderboard , idx )
                        else -- the score wasn't higher, so no need to insert a new score
                            insert = false
                        end

                    end

                end

                if insert then

                    for idx , winner_table in ipairs( leaderboard ) do
                        if score > winner_table [ 1 ] then 
                            table.insert( leaderboard , idx , { score , winner_name } )
                            if idx == 1 then
                                new_highscore = true
                            end
                            break
                        end
                    end

                end
            end

            --arena_lib.change_arena_property(winner_name, 'wormball', arena.name, "singleplayer_leaderboard", leaderboard)

            --arena_lib.change_arena_property('wormball' , 'wormball' , arena.name , "singleplayer_leaderboard" , leaderboard )

            --save (update) the highscore data to disk
            arena.singleplayer_leaderboard = leaderboard

                        
            local success, msg = wormball.leaderboard.save_highscores(arena.name,leaderboard)
            --minetest.chat_send_all(tostring(success))
            --minetest.chat_send_all(msg)
                        
            --local serial = minetest.serialize(arena.singleplayer_leaderboard)

            --storage:set_string(arena.name .. "_highscores", serial )






            if new_highscore then
                arena_lib.HUD_send_msg_all("title", arena, T("New High Score!"), 2 ,'sumo_win',0xAEAE00)
                minetest.after(2,function()
                    if minetest.get_modpath("aes_xp") then
                        --register some achievements
                        aes_xp.add_achievement("Wormball Master", 150, 100, 0, T("Beat the Highscore"), "wormball_win.png")
                        aes_xp.achieve(winner_name,"Wormball Master")
                    end
                end)
            else
                arena_lib.HUD_send_msg_all("title", arena, T('Game Over!'), 2 ,'sumo_win',0xAEAE00)
                minetest.after(3,function()
                    if minetest.get_modpath("aes_xp") then
                        --register some achievements
                        aes_xp.add_achievement("Worming Along", 10, 0, 0, "Play a round of \nsingleplayer wormball", "magiccompass_wormball.png")
                        aes_xp.achieve(winner_name,"Worming Along")
                    end
                end)
                if score > 50 then
                    minetest.after(4,function()
                        if minetest.get_modpath("aes_xp") then
                            --register some achievements
                            aes_xp.add_achievement("Mega Worm", 50, 40, 0, "Eat more than 50 \npowerpoints in singleplayer\nwormball", "magiccompass_wormball.png")
                            aes_xp.achieve(winner_name,"Mega Worm")
                        end
                    end)
                elseif score > 20 then
                    minetest.after(4,function()
                        if minetest.get_modpath("aes_xp") then
                            --register some achievements
                            aes_xp.add_achievement("Small Worm", 15, 10, 0, "Eat more than 20 \npowerpoints in singleplayer\nwormball", "magiccompass_wormball.png")
                            aes_xp.achieve(winner_name,"Small Worm")
                        end
                    end)
                elseif score > 5 then
                    minetest.after(4,function()
                        if minetest.get_modpath("aes_xp") then
                            --register some achievements
                            aes_xp.add_achievement("Baby Wiggler", 5, 5, 0, "Eat more than 5 \npowerpoints in singleplayer\nwormball", "magiccompass_wormball.png")
                            aes_xp.achieve(winner_name,"Baby Wiggler")
                        end
                    end)
                end
            end

            minetest.after( 3 , function( arena , score , leaderboard ) 

                arena_lib.HUD_send_msg_all("title", arena , T("You had @1 pts", score).."!", 3 ,'sumo_win',0xAEAE00)

                

                minetest.after( 3 , function( arena , score , leaderboard ) 

                    for pl_name , stats in pairs(arena.players_and_spectators) do
                        wormball.show_singleplayer_leaderboard( arena.name , pl_name )
                    end
                    
                end , arena , score , leaderboard )

                

            end , arena , score , leaderboard )

        else --arena.mode == 'multiplayer'

            arena_lib.HUD_send_msg_all("title", arena, T("New High Score!"), 2 ,'sumo_win',0xAEAE00)

            table.insert( arena.multi_scores , 1 , { arena.players[ winner_name ] .score , winner_name } ) 

            local scores = arena.multi_scores

            -- add some multiplayer achievements
            local len = 0
            for j,k in pairs(scores) do
                len = len + 1
            end
            if len == 2 then
                if minetest.get_modpath("aes_xp") then
                    --register some achievements
                    aes_xp.add_achievement("Worm vs Worm", 10, 5, 0, T("Win a 2 player Wormball match"), "wormball_win.png")
                    aes_xp.achieve(winner_name,"Worm vs Worm")
                end
            elseif len < 4 then
                if minetest.get_modpath("aes_xp") then
                    --register some achievements
                    aes_xp.add_achievement("Sir LongWorm", 30, 20, 0, T("Win a small Wormball match"), "wormball_win.png")
                    aes_xp.achieve(winner_name,"Sir LongWorm")
                end
            else
                if minetest.get_modpath("aes_xp") then
                    --register some achievements
                    aes_xp.add_achievement("King of the Worms", 70, 40, 0, T("Win a large Wormball match"), "wormball_win.png")
                    aes_xp.achieve(winner_name,"King of the Worms")
                end
            end


            minetest.after( 2 , function( arena , scores ) 
                
                for pl_name , stats in pairs(arena.players_and_spectators) do
                    wormball.show_multi_scores( arena , pl_name , scores )
                end
                    
                -- show the scoreboard here
                
            end , arena , scores )

        end

    end

end)


