
arena_lib.register_editor_section("wormball", {

    name = "wormball_settings",
    icon = "wormball_menu.png",
    hotbar_message = "Wormball Arena Settings",
    give_items = function(itemstack, user, arena)


        -- add the pos ents
        local pos1 = arena.area_to_clear_after_game_pos_1
        local pos2 = arena.area_to_clear_after_game_pos_2
        if type(pos1.y) == "number" and 
            type(pos1.x) == "number" and 
            type(pos1.z) == "number" and 
            type(pos2.y) == "number" and 
            type(pos2.x) == "number" and 
            type(pos2.z) == "number" then

                
        
            local staticdata = minetest.write_json({
                _arena = arena.name,
                _pos = 1,
            })

            minetest.add_entity(pos1, "wormball:pos_ent", staticdata)

            local staticdata = minetest.write_json({
                _arena = arena.name,
                _pos = 2,
            })

            local obj = minetest.add_entity(pos2, "wormball:pos_ent", staticdata)
        end


        local tbl = {"wormball:pos1","wormball:pos2","arena_lib:settings_rename_author","arena_lib:settings_properties","arena_lib:settings_delete",}
        return tbl
    end,
})




minetest.register_entity("wormball:pos_ent",{
    initial_properties = {
        physical = false,
        use_texture_alpha = true,
        collisionbox = {-0.01, -0.01, -0.01, 0.01, 0.01, 0.01},
        visual = "cube",
        textures = {"blank.png","blank.png","blank.png","blank.png","blank.png","blank.png"},
        visual_size = {x = 1.3, y = 1.3, z = 1.3},
        static_save = false,
    },
    on_activate = function(self, staticdata, dtime_s)
        if staticdata ~= "" and staticdata ~= nil then
            local data = minetest.parse_json(staticdata) or {}
            if data._arena then self._arena = data._arena end
            if data._pos then self._pos = data._pos end
        end
    end,
    on_step = function(self, dtime, moveresult)
        
        self._timer = self._timer + dtime

        if self._timer > .1 then

            if self._arena == "" then 
                self.object:remove()
                return
            end

            if not arena_lib.get_arena_by_name('wormball', self._arena ) then 
                self.object:remove()
                return
            end

            local arena_id, arena = arena_lib.get_arena_by_name('wormball', self._arena )

            if arena.enabled == true then
                self.object:remove()
                return
            end

            if self._pos == 1 then
                self.object:set_properties({textures = {"wormball_loc1.png","wormball_loc1.png","wormball_loc1.png","wormball_loc1.png","wormball_loc1.png","wormball_loc1.png",}})
                local objpos = vector.floor(self.object:get_pos())
                local arpos = arena.area_to_clear_after_game_pos_1
                if not(objpos.x == arpos.x and objpos.y == arpos.y and objpos.z == arpos.z)  then
                    self.object:remove()
                    return
                end
            end
            if self._pos == 2 then
                self.object:set_properties({textures = {"wormball_loc2.png","wormball_loc2.png","wormball_loc2.png","wormball_loc2.png","wormball_loc2.png","wormball_loc2.png",}})
                local objpos = vector.floor(self.object:get_pos())
                local arpos = arena.area_to_clear_after_game_pos_2
                if not(objpos.x == arpos.x and objpos.y == arpos.y and objpos.z == arpos.z)  then
                    
                    self.object:remove()
                    return
                end
            end
                
        end
        
    end,

    _arena = "",
    _pos = 0,
    _timer = 0.0,

})







minetest.register_tool("wormball:pos1",{

    description = "Area to Clear Pos 1",
    inventory_image = "wormball_pos1.png",
    wield_image = "wormball_pos1.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,

    on_use = function(itemstack, user, pointed_thing)

        local mod = user:get_meta():get_string("arena_lib_editor.mod")
        local arena_name = user:get_meta():get_string("arena_lib_editor.arena")
        local pos = vector.round(user:get_pos())
        local pos_string = '{ x = '..pos.x .. ", y = " .. pos.y .. ", z = " .. pos.z .. "}" 
        
        arena_lib.change_arena_property(user:get_player_name(), 'wormball', arena_name, "area_to_clear_after_game_pos_1", pos_string , true)
        minetest.chat_send_player(user:get_player_name(), "Pos_1 has been set to :"..dump(pos))  

        
        local staticdata = minetest.write_json({
            _arena = arena_name,
            _pos = 1,
        })

        minetest.add_entity(pos, "wormball:pos_ent", staticdata)

    end
})


minetest.register_tool("wormball:pos2",{

    description = "Area to Clear Pos 2",
    inventory_image = "wormball_pos2.png",
    wield_image = "wormball_pos2.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,

    on_use = function(itemstack, user, pointed_thing)

        local mod = user:get_meta():get_string("arena_lib_editor.mod")
        local arena_name = user:get_meta():get_string("arena_lib_editor.arena")
        local pos = vector.round(user:get_pos())
        local pos_string = '{ x = '..pos.x .. ", y = " .. pos.y .. ", z = " .. pos.z .. "}" 
        arena_lib.change_arena_property(user:get_player_name(), 'wormball', arena_name, "area_to_clear_after_game_pos_2", pos_string , true)        minetest.chat_send_player(user:get_player_name(), "Pos_2 has been set to :"..dump(pos)) 
        minetest.chat_send_player(user:get_player_name(), "Pos_2 has been set to :"..dump(pos)) 
        local staticdata = minetest.write_json({
            _arena = arena_name,
            _pos = 2,
        })

        minetest.add_entity(pos, "wormball:pos_ent", staticdata)
    end
})


